package Graphics1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Fleps_000 on 04.06.2014.
 */
public class Mnogoug extends JPanel implements ActionListener {
    public double phi = 10 * Math.PI / 180, psi = 10 * Math.PI / 180;
    int otx = 500, oty = 500;
    int k = 4;
    String str = "", strMove = "";

    public class Points3D {
        double x;
        double y;
        double z;

        public Points3D(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        Points3D() {
        }
    }

    Points3D[] Mnogoug2D = {
                    new Points3D(10, -16, 0),
                    new Points3D(35, 1, 0),
                    new Points3D(35, 10, 0),
                    new Points3D(1, 20, 0),
                    new Points3D(10, -16, 0),
    };
    Points3D[] Mnogoug3D = {
            new Points3D(-20, 10, 40),
            new Points3D(20, 10, 40),
            new Points3D(20, 30, 20),
            new Points3D(-20, 10, 40),
            new Points3D(20, 10, 40),
            new Points3D(20, -10, 10),
            new Points3D(-20, 10, 40),
            new Points3D(20, -10, 10),
            new Points3D(20, 30, 20),
            new Points3D(20, 10, 40),
            new Points3D(-20, 10, 40),
            new Points3D(20, -10, 10),
            new Points3D(20, 30, 20),
    };
    Points3D[] laba17 = {
            new Points3D(-10, 20, -30),
            new Points3D(10, 20, -30),
            new Points3D(20, 10, -30),
            new Points3D(20, -10, -30),
            new Points3D(10, -20, -30),
            new Points3D(-10, -20, -30),
            new Points3D(-20, -10, -30),
            new Points3D(-20, 10, -30),
            new Points3D(-10, 20, -30),
            new Points3D(-10, 20, -80),
            new Points3D(10, 20, -30),
            new Points3D(10, 20, -80),
            new Points3D(20, 10, -30),
            new Points3D(20, 10, -80),
            new Points3D(20, -10, -30),
            new Points3D(20, -10, -80),
            new Points3D(10, -20, -30),
            new Points3D(10, -20, -80),
            new Points3D(-10, -20, -30),
            new Points3D(-10, -20, -80),
            new Points3D(-20, -10, -30),
            new Points3D(-20, -10, -80),
            new Points3D(-20, 10, -30),
            new Points3D(-20, 10, -80),
            new Points3D(-10, 20, -80),
            new Points3D(10, 20, -80),
            new Points3D(20, 10, -80),
            new Points3D(20, -10, -80),
            new Points3D(10, -20, -80),
            new Points3D(-10, -20, -80),
            new Points3D(-20, -10, -80),
            new Points3D(-20, 10, -80),
            new Points3D(-10, 20, -80),
    };

    Mnogoug() {
        super();
        setOpaque(true);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.WHITE);
        setFocusable(true);
        JButton DrawMnog2D = new JButton("2D многоугольник");
        JButton DrawMnog3D = new JButton("3D многоугольник");
        JButton TurnLeftButton = new JButton("Влево");
        JButton TurnRightButton = new JButton("Вправо");
        JButton TurnUpButton = new JButton("Вверх");
        JButton TurnDownButton = new JButton("Вниз");
        JButton laba17Button = new JButton("17 лаба");
        add(DrawMnog2D);
        DrawMnog2D.addActionListener(this);
        add(laba17Button);
        laba17Button.addActionListener(this);
        add(DrawMnog3D);
        DrawMnog3D.addActionListener(this);
        add(TurnLeftButton);
        TurnLeftButton.addActionListener(this);
        add(TurnRightButton);
        TurnRightButton.addActionListener(this);
        add(TurnUpButton);
        TurnUpButton.addActionListener(this);
        add(TurnDownButton);
        TurnDownButton.addActionListener(this);
        this.setLayout(null);

        TurnLeftButton.setBounds(50, 75, 120, 25);
        TurnRightButton.setBounds(50, 125, 120, 25);
        TurnUpButton.setBounds(50, 175, 120, 25);
        TurnDownButton.setBounds(50, 225, 120, 25);
        DrawMnog3D.setBounds(50, 275, 170, 25);
        laba17Button.setBounds(50, 325, 170, 25);
        DrawMnog2D.setBounds(50,375,170,25);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 800);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        str = e.getActionCommand();
        switch (str) {
            case "2D многоугольник":
                strMove = str; break;
            case "3D многоугольник":
                strMove = str; break;
            case "17 лаба":
                strMove = str; break;
        }
        repaint();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawString("Вы выбрали: " + str, 200, 80);
        switch (str) {
            case "Влево":
                phi += 0.2;
                break;
            case "Вправо":
                phi -= 0.2;
                break;
            case "Вверх":
                psi += 0.2;
                break;
            case "Вниз":
                psi -= 0.2;
                break;
            case "2D многоугольник":
                break;
            case "3D многоугольник":
                break;
            case "17 лаба":
                break;
        }
        switch (strMove) {
            case "2D многоугольник":
                draw(Mnogoug2D,g);
                break;
            case "3D многоугольник":
                draw(Mnogoug3D,g);
                break;
            case "17 лаба":
                draw(laba17,g);
                break;
        }
    }

    public static double scal(Points3D a, Points3D b)
    {

        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    private void draw(Points3D Mnogoug[],Graphics g) {
        float U0, U1, V0, V1;
        Points3D e1 = new Points3D();
        e1.x = Math.cos(phi);
        e1.y = Math.sin(phi);
        e1.z = 0.0;
        Points3D e2 = new Points3D();
        e2.x = -Math.sin(phi) * Math.sin(psi);
        e2.y = Math.cos(phi) * Math.sin(psi);
        e2.z = Math.cos(psi);
        for (int i = 0; i < Mnogoug.length-1; i++) {
            U0 = (float)scal(Mnogoug[i], e1) * k;
            V0 = (float)scal(Mnogoug[i], e2) * k;
            U1 = (float)scal(Mnogoug[i+1], e1) * k;
            V1 = (float)scal(Mnogoug[i+1], e2) * k;
            int tmp = 500;
            g.drawLine((int) (tmp + U0),(int) (tmp+V0), (int) (tmp+U1), (int) (tmp + V1));
        }
    }
}