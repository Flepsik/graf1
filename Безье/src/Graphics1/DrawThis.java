package Graphics1; /**
 * Created by Fleps_000 on 09.04.2014.
 */

import javax.swing.*;

public class DrawThis {
    private Mnogoug holst;

    DrawThis() {
        holst = new Mnogoug();

        JFrame mainFrame = new JFrame("Draw points");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.add(holst);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new DrawThis();
            }
        });
    }
}