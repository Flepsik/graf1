/**
 * Created by Fleps_000 on 09.04.2014.
 */

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class Draw {
    private Holst holst;

    Draw() {
        holst = new Holst();

        JFrame mainFrame = new JFrame("Draw points");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.add(holst);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Draw();
            }
        });
    }
}