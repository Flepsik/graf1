/**
 * Created by Fleps_000 on 09.04.2014.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class Holst extends JPanel implements ActionListener {
    int stx, sty;
    String str = "";

    private class Points3D {
        double x;
        double y;
        double z;

        public Points3D(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        Points3D() {
        }
    }

    Points3D[][] Figure = {
            {
                    new Points3D(0, -3, 1),
                    new Points3D(0, 2, 3),
                    new Points3D(0, 0, -2)
            },
            {
                    new Points3D(1, 0, 0),
                    new Points3D(1, -4, -1),
                    new Points3D(1, 0, -2)
            },
            {
                    new Points3D(2, 0, -1),
                    new Points3D(2, 0, -1),
                    new Points3D(2, 2, -2)
            }
    };
    int otx = 500, oty = 500;
    ArrayList<Points3D> arrayList = new ArrayList<Points3D>();
    Points3D e1 = new Points3D(), e2 = new Points3D();
    public double phi = 10 * Math.PI / 180, psi = 10 * Math.PI / 180;
    Points3D begin = new Points3D();
    int tmp = 200;
    int k = 100;
    String strMove = "";

    Holst() {
        super();
        setOpaque(true);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.WHITE);
        setFocusable(true);
        JButton DrawSplineButton = new JButton("Сплайн");
        JButton DrawBezieButton = new JButton("Безье");
        JButton TurnLeftButton = new JButton("Влево");
        JButton TurnRightButton = new JButton("Вправо");
        JButton TurnUpButton = new JButton("Вверх");
        JButton TurnDownButton = new JButton("Вниз");
        JButton Surface3DButton = new JButton("Поверхность 3D");
        JButton SurfaceBezieButton = new JButton("Поверхность Безье");
        add(DrawSplineButton);
        DrawSplineButton.addActionListener(this);
        add(Surface3DButton);
        Surface3DButton.addActionListener(this);
        add(SurfaceBezieButton);
        SurfaceBezieButton.addActionListener(this);
        add(DrawBezieButton);
        DrawBezieButton.addActionListener(this);
        add(TurnLeftButton);
        TurnLeftButton.addActionListener(this);
        add(TurnRightButton);
        TurnRightButton.addActionListener(this);
        add(TurnUpButton);
        TurnUpButton.addActionListener(this);
        add(TurnDownButton);
        TurnDownButton.addActionListener(this);
        this.setLayout(null);
        DrawSplineButton.setLocation(50, 25);
        DrawSplineButton.setSize(120, 25);
        TurnLeftButton.setBounds(50, 75, 120, 25);
        TurnRightButton.setBounds(50, 125, 120, 25);
        TurnUpButton.setBounds(50, 175, 120, 25);
        TurnDownButton.setBounds(50, 225, 120, 25);
        DrawBezieButton.setBounds(50, 275, 120, 25);
        Surface3DButton.setBounds(50, 325, 170, 25);
        SurfaceBezieButton.setBounds(50, 375, 170, 25);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                arrayList.add(new Points3D(e.getX(), e.getY(), 0));
                repaint();
            }
        });

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        str = ae.getActionCommand();
        switch (str) {
            case "Поверхность 3D":
                strMove = str;
                break;
            case "Сплайн":
                strMove = str;
                break;
            case "Безье":
                strMove = str;
                break;
            case "Поверхность Безье":
                strMove = str;
                break;
        }
        repaint();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 800);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawString("Вы выбрали: " + str, 200, 80);
        switch (str) {
            case "Влево":
                phi += 0.2;
                break;
            case "Вправо":
                phi -= 0.2;
                break;
            case "Вверх":
                psi += 0.2;
                break;
            case "Вниз":
                psi -= 0.2;            break;
            case "Сплайн":
                break;
            case "Безье":
                break;
            case "Поверхность 3D":
                break;
            case "Поверхность Безье":
                break;

            default:
                for (int i = 0; i < arrayList.size(); i++) {
                    g.drawRoundRect((int) arrayList.get(i).x, (int) arrayList.get(i).y, 4, 4, 4, 4);
                }
        }
        switch (strMove) {
            case "Сплайн":
                drawSpline(g);
                break;
            case "Безье":
                drawBezie(g);
                break;
            case "Поверхность 3D":
                drawSurface3D(g);
                break;
            case "Поверхность Безье":
                drawBezieSurface(g);
                break;
        }


    }

    double bezie(double u, double v, double[][] a) {

        int[] cnk = {1, 2, 1};
        double s = 0;
        double[] U0 = {1.0, u, u * u};
        double[] V0 = {1.0, v, v * v};
        double[] U1 = {(1.0 - u) * (1.0 - u), 1.0 - u, 1.0};
        double[] V1 = {(1.0 - v) * (1.0 - v), 1.0 - v, 1.0};
        for (int i = 0; i <= 2; i++)

            for (int j = 0; j <= 2; j++) {
                s += a[i][j] * cnk[i] * U0[i] * U1[i] * cnk[j] * V0[j] * V1[j];
            }
        return s;
    }


    double x_bezie(double u, double v) {
        double[][] X = {
                {Figure[0][0].x, Figure[0][1].x, Figure[0][2].x},
                {Figure[1][0].x, Figure[1][1].x, Figure[1][2].x},
                {Figure[2][0].x, Figure[2][1].x, Figure[2][2].x}};
        double s = bezie(u, v, X);
        return s;
    }

    double y_bezie(double u, double v) {
        double[][] Y = {
                {Figure[0][0].y, Figure[0][1].y, Figure[0][2].y},
                {Figure[1][0].y, Figure[1][1].y, Figure[1][2].y},
                {Figure[2][0].y, Figure[2][1].y, Figure[2][2].y}};
        double s = bezie(u, v, Y);
        return s;
    }

    double z_bezie(double u, double v) {
        double[][] Z = {
                {Figure[0][0].z, Figure[0][1].z, Figure[0][2].z},
                {Figure[1][0].z, Figure[1][1].z, Figure[1][2].z},
                {Figure[2][0].z, Figure[2][1].z, Figure[2][2].z}};
        double s = bezie(u, v, Z);
        return s;
    }

    private void drawBezieSurface(Graphics g) {
        double j, i;
        e1.x = Math.cos(phi);
        e1.y = Math.sin(phi);
        e1.z = 0.0;

        e2.x = -Math.sin(phi) * Math.sin(psi);
        e2.y = Math.cos(phi) * Math.sin(psi);
        e2.z = Math.cos(psi);

        for (i = 0; i <= 1; i += 0.1) {
            j = 0;
            begin.x = x_bezie(i, j);
            begin.y = y_bezie(i, j);
            begin.z = z_bezie(i, j);
            double U0 = Skall(begin, e1) * k;
            double V0 = Skall(begin, e2) * k;
            for (j = 0; j <= 1; j += 0.1) {
                begin.x = x_bezie(i, j);
                begin.y = y_bezie(i, j);
                begin.z = z_bezie(i, j);

                double U1 = Skall(begin, e1) * k;
                double V1 = Skall(begin, e2) * k;
                g.drawLine((int) (otx + U0), (int) (oty + V0), (int) (otx + U1), (int) (oty + V1));
                V0 = V1;
                U0 = U1;
            }
        }


        for (j = 0; j < 1; j += 0.1) {
            i = 0;
            begin.x = x_bezie(i, j);
            begin.y = y_bezie(i, j);
            begin.z = z_bezie(i, j);
            double U0 = Skall(begin, e1) * k;
            double V0 = Skall(begin, e2) * k;
            for (i = 0; i <= 1; i += 0.1) {
                begin.x = x_bezie(i, j);
                begin.y = y_bezie(i, j);
                begin.z = z_bezie(i, j);
                double U1 = Skall(begin, e1) * k;
                double V1 = Skall(begin, e2) * k;
                g.drawLine((int) (otx + U0), (int) (oty + V0), (int) (otx + U1), (int) (oty + V1));
                V0 = V1;
                U0 = U1;
            }
        }
    }

    public static double x(double y, double z) {
        return y;
    }

    public static double y(double x, double z) {
        return z;
    }

    public static double z(double y, double x) {
        return (Math.cos(x * x * x * Math.PI * (Math.cos(y * y))) * 1.5);
    }

    private void drawSurface3D(Graphics g) {
        double j, i;
        e1.x = Math.cos(phi);
        e1.y = Math.sin(phi);
        e2.x = -Math.sin(phi) * Math.sin(psi);
        e2.y = Math.cos(phi) * Math.sin(psi);
        e2.z = Math.cos(psi);
        for (i = 0; i <= 1; i += 0.1) {
            j = 0;
            begin.x = x(i, j);
            begin.y = y(i, j);
            begin.z = z(begin.x, begin.y);

            double u0 = Skall(begin, e1) * k;
            double v0 = Skall(begin, e2) * k;
            for (j = 0; j <= 1; j += 0.1) {
                begin.x = x(i, j);
                begin.y = y(i, j);
                begin.z = z(begin.x, begin.y);

                double u1 = Skall(begin, e1) * k;
                double v1 = Skall(begin, e2) * k;

                g.drawLine((int) (tmp + u0 + 150), (int) (tmp + v0 + 150), (int) (tmp + u1 + 150), (int) (tmp + v1 + 150));
                u0 = u1;
                v0 = v1;

            }
        }


        for (j = 0; j < 1; j += 0.1) {
            i = 0;
            begin.x = x(i, j);
            begin.y = y(i, j);
            begin.z = z(begin.x, begin.y);

            float u0 = (float) Skall(begin, e1) * k;
            float v0 = (float) Skall(begin, e2) * k;

            for (i = 0; i <= 1; i += 0.1) {
                begin.x = x(i, j);
                begin.y = y(i, j);
                begin.z = z(begin.x, begin.y);

                float u1 = (float) Skall(begin, e1) * k;
                float v1 = (float) Skall(begin, e2) * k;
                g.drawLine((int) (tmp + u0 + 150), (int) (tmp + v0 + 150), (int) (tmp + u1 + 150), (int) (tmp + v1 + 150));
                u0 = u1;
                v0 = v1;

            }
        }

    }

    private static double Skall(Points3D point1, Points3D point2) {
        ;
        return point1.x * point2.x + point1.y * point2.y + point1.z * point2.z;
    }

    private void drawSpline(Graphics g) {
        float u0, v0, u1, v1;
        e1.x = Math.cos(phi);
        e1.y = Math.sin(phi);
        e2.x = -Math.sin(phi) * Math.sin(psi);
        e2.y = Math.cos(phi) * Math.sin(psi);
        begin.x = (arrayList.get(0).x + arrayList.get(1).x) / 2;
        begin.y = (arrayList.get(0).y + arrayList.get(1).y) / 2;
        u0 = (float) Skall(begin, e1);
        v0 = (float) Skall(begin, e2);
        u0 += tmp;
        v0 += tmp;
        for (int i = 0; i < arrayList.size() - 2; i++) {
            for (float u = 0.0F; u < 1.0; u += 0.1F) {
                begin.x = 0.5F * (1 - u) * (1 - u) * arrayList.get(i).x + (0.75F - (u - 0.5F) * (u - 0.5F)) * arrayList.get(i + 1).x + 0.5F * u * u * arrayList.get(i + 2).x;
                begin.y = 0.5F * (1 - u) * (1 - u) * arrayList.get(i).y + (0.75F - (u - 0.5F) * (u - 0.5F)) * arrayList.get(i + 1).y + 0.5F * u * u * arrayList.get(i + 2).y;
                u1 = (float) Skall(begin, e1);
                v1 = (float) Skall(begin, e2);
                u1 += tmp;
                v1 += tmp;
                g.drawLine((int) u0, (int) v0, (int) u1, (int) v1);
                u0 = u1;
                v0 = v1;
            }
        }
    }

    private void drawBezie(Graphics g) {
        double ax, ay, t, sx, sy, tau;
        double u0, v0, u1, v1;
        e1.x = Math.cos(phi);
        e1.y = Math.sin(phi);
        e2.x = -Math.sin(phi) * Math.sin(psi);
        e2.y = Math.cos(phi) * Math.sin(psi);
        begin.x = arrayList.get(0).x;
        begin.y = arrayList.get(0).y;
        u0 = (float) Skall(begin, e1);
        v0 = (float) Skall(begin, e2);
        u0 += tmp;
        v0 += tmp;
        int size = arrayList.size();
        for (t = 0; t < 0.5; t += 0.01F) {
            ax = 1.0F;
            ay = 1.0F;
            sx = (float) arrayList.get(0).x;
            sy = (float) arrayList.get(0).y;
            tau = 1.0F;
            for (int i = 1; i < size; i++) {
                tau = tau * (1 - t);
                ax = ax * t * (size - i) / (i * (1 - t));
                ay = ay * t * (size - i) / (i * (1 - t));

                sx = (float) (sx + ax * arrayList.get(i).x);
                sy = (float) (sy + ay * arrayList.get(i).y);
            }
            sx = sx * tau;
            sy = sy * tau;
            begin.x = sx;
            begin.y = sy;
            u1 = (int) Skall(begin, e1);
            v1 = (int) Skall(begin, e2);
            u1 += tmp;
            v1 += tmp;
            g.drawLine((int) u0, (int) v0, (int) u1, (int) v1);
            u0 = u1;
            v0 = v1;
        }
        begin.x = arrayList.get(size - 1).x;
        begin.y = arrayList.get(size - 1).y;
        u0 = (int) Skall(begin, e1);
        v0 = (int) Skall(begin, e2);
        u0 += tmp;
        v0 += tmp;


        for (t = 1; t >= 0.5; t -= 0.01F) {
            ax = 1.0F;
            ay = 1.0F;
            sx = arrayList.get(size - 1).x;
            sy = arrayList.get(size - 1).y;
            tau = 1.0F;
            for (int i = size - 2; i >= 0; i--) {
                tau = tau * t;
                ax = ax * (1 - t) * (i + 1) / ((size - 1 - i) * t);
                ay = ay * (1 - t) * (i + 1) / ((size - 1 - i) * t);
                sx = sx + ax * arrayList.get(i).x;
                sy = sy + ay * arrayList.get(i).y;
            }
            sx = sx * tau;
            sy = sy * tau;
            begin.x = sx;
            begin.y = sy;
            u1 = Skall(begin, e1);
            v1 = Skall(begin, e2);
            u1 += tmp;
            v1 += tmp;
            g.drawLine((int) u0, (int) v0, (int) u1, (int) v1);
            u0 = u1;
            v0 = v1;
        }
    }
}
